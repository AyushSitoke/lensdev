package com.lti.lens.Controllers;

import static org.junit.Assert.*;

import org.junit.Test;

public class WebTest {

	@Test
	public void test() {
		TestWebClass junit = new TestWebClass();
		int res = junit.add(5, 5);
		assertEquals(11 ,res);
	}

	@Test
	public void test2() {
		TestWebClass junit = new TestWebClass();
		int res = junit.add(4, 5);
		assertEquals(9,res);
	}
	@Test
	public void test3() {
		TestWebClass junit = new TestWebClass();
		int res = junit.add(6, 6);
		assertEquals(12 ,res);
	}
	@Test
	public void test4() {
		TestWebClass junit = new TestWebClass();
		int res = junit.add(4, 4);
		assertEquals(8 ,res);
	}
	@Test
	public void test5() {
		TestWebClass junit = new TestWebClass();
		int res = junit.add(6, 8);
		assertEquals(14 ,res);
	}
}